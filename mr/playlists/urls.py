from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:id>', views.playlist, name='playlist'),
    path('populate', views.populate_playlists, name='populate_playlists')
]
