from django.db import models
from songs.models import Song


class Playlist(models.Model):
    """Model representing a playlist of songs"""
    name = models.CharField(max_length=50)
    songs = models.ManyToManyField(Song, related_name='playlists')

    def __str__(self):
        return self.name
