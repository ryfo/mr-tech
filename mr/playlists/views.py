from django.shortcuts import render, redirect
from .models import Playlist
from songs.models import Song


def index(request):
    """Return all playlists"""
    playlists = Playlist.objects.all()
    context = {
        'playlists': playlists
    }

    return render(request, 'index.html', context=context)


def playlist(request, id):
    """Return a specific playlist and the contained songs"""
    playlist = Playlist.objects.get(pk=id)

    return render(request, 'playlist.html', context={'playlist': playlist})


def populate_playlists(request):
    """Populate both playlists"""
    populate_rock_playlist()
    populate_nineties_playlist()

    return redirect('index')


def populate_rock_playlist():
    # Create the rock playlist first
    rock_name = "Rock"
    rock_playlist = Playlist.objects.filter(name=rock_name)
    if rock_playlist:
        rock_playlist.delete()

    new_rock_playlist = Playlist(name=rock_name)
    new_rock_playlist.save()

    # Add rock songs to playlist
    rock_songs = Song.objects.filter(genre__name=rock_name)
    for song in rock_songs:
        new_rock_playlist.songs.add(song)


def populate_nineties_playlist():
    # Create 90's playlist
    nineties_name = "90s"
    nineties_playlist = Playlist.objects.filter(name=nineties_name)
    if nineties_playlist:
        nineties_playlist.delete()

    new_nineties_playlist = Playlist(name=nineties_name)
    new_nineties_playlist.save()

    # Add 90's songs to playlist
    nineties_songs = Song.objects.filter(year__gte=1990, year__lte=1999)
    for song in nineties_songs:
        new_nineties_playlist.songs.add(song)
