from django.contrib import admin
from .models import Playlist
from songs.models import Song


class SongInline(admin.TabularInline):
    """Inline admin editor for songs within a playlist"""
    model = Playlist.songs.through
    extra = 0


@admin.register(Playlist)
class PlaylistAdmin(admin.ModelAdmin):
    """Playlist admin editor"""
    list_display = ['name']
    fields = ['name']
    inlines = [SongInline]
