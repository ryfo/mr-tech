from django import forms


class UploadSongFileForm(forms.Form):
    """Form for uploading a file"""
    file = forms.FileField()