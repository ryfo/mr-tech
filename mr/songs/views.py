from django.shortcuts import render, redirect
from .forms import UploadSongFileForm
from .models import Song, Artist, Genre
from datetime import timedelta
import json
import tempfile


def upload_songs(request):
    """Upload JSON file for importing songs"""
    if request.method == 'POST':
        form = UploadSongFileForm(request.POST, request.FILES)
        if form.is_valid():
            # Use file chunking for better performance with larger files
            with tempfile.TemporaryFile() as destination:
                for chunk in request.FILES['file'].chunks():
                    destination.write(chunk)

                # Reset file position to beginning before reading
                destination.seek(0)
                import_json(destination.read())

            return redirect('index')
    else:
        form = UploadSongFileForm()
    return render(request, 'upload.html', {'form': form})


def import_json(raw_data):
    json_data = json.loads(raw_data)

    # Validate and import songs
    for song in json_data:
        if is_song_valid(song):
            import_song(song)


def is_song_valid(song_json):
    """Validate that a song meets import requirements"""
    # check that all fields are present
    if (song_json["Name"] and song_json["Artist"] and
        song_json["Year"] and song_json["Genre"] and
        song_json["BPM"] and song_json["Time"]):

        # check for invalid artists
        if (song_json["Artist"].lower() == "bee gees" or
            song_json["Artist"].lower() == "linkin park"):
            return False

        # check for minimum BPM
        if song_json["BPM"] < 100:
            return False

        # check for minimum duration
        if song_json["Time"] < 60:
            return False

        return True
    else:
        return False


def import_song(song_json):
    """Add a song to the database, from a JSON definition"""
    # Get or import artist
    artist = Artist.objects.filter(name=song_json["Artist"])
    if artist:
        artist = artist[0]
    else:
        artist = Artist(name=song_json["Artist"])
        artist.save()

    # Get or import genre
    genre = None
    for temp_genre in Genre.objects.all():
        if temp_genre.name.lower() in song_json["Genre"].lower():
            genre = temp_genre

    if not genre:
        genre = Genre(name=song_json["Genre"])
        genre.save()

    # Check if the song already exists in the database
    existing_song = Song.objects.filter(
        title=song_json["Name"],
        artist__name=song_json["Artist"])

    # Update or import the song itself
    if existing_song:
        existing_song = existing_song[0]
        existing_song.year = song_json["Year"]
        existing_song.genre = genre
        existing_song.bpm = song_json["BPM"]
        existing_song.duration = timedelta(seconds=song_json["Time"])
        existing_song.save()
    else:
        song = Song(title=song_json["Name"],
                    artist=artist,
                    year=song_json["Year"],
                    genre=genre,
                    bpm=song_json["BPM"],
                    duration=timedelta(seconds=song_json["Time"]))

        song.save()
