from django.test import TestCase
from .views import is_song_valid


class TestSongValidation(TestCase):
    def testRejectIfBpmTooLow(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 99,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfDurationTooLow(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 50,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfArtistBeeGees(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "Bee Gees",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))
        
    def testRejectIfArtistLinkinPark(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "LInkIN ParK",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfNameMissing(self):
        song = {
            "Name": "",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfArtistMissing(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfGenreMissing(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": 2009
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfBPMMissing(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "Size": 8634363,
            "Time": 230,
            "Year": 2009,
            "BPM": ""
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfTimeMissing(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Year": 2009,
            "Time": ""
        }

        self.assertFalse(is_song_valid(song))

    def testRejectIfYearMissing(self):
        song = {
            "Name": "All the Right Moves",
            "Artist": "OneRepublic",
            "Composer": "Ryan Tedder",
            "Album": "Waking Up (Deluxe Version)",
            "Genre": "Rock",
            "BPM": 146,
            "Size": 8634363,
            "Time": 230,
            "Year": ""
        }

        self.assertFalse(is_song_valid(song))