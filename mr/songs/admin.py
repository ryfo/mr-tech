from django.contrib import admin
from .models import Artist, Genre, Song


# Register your models here.
admin.site.register(Genre)
admin.site.register(Artist)


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    list_display = ['title', 'artist', 'year', 'genre', 'duration', 'bpm']
    list_filter = ('artist', 'genre', 'year')
    readonly_fields = ['created_at', 'updated_at']
