from django.db import models


class Artist(models.Model):
    """Model representing a single artist"""
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Genre(models.Model):
    """Model representing a single genre"""
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Song(models.Model):
    """Model representing a single song"""
    title = models.CharField(max_length=50)
    artist = models.ForeignKey('Artist', on_delete=models.CASCADE, null=False)
    year = models.PositiveIntegerField()
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE, null=False)
    bpm = models.PositiveIntegerField()
    duration = models.DurationField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title} by {self.artist}'

    class Meta:
        ordering = ['bpm']

